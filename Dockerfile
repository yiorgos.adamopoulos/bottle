FROM python:3.11
WORKDIR /usr/app/bottle
COPY requirements.txt .
RUN pip --no-cache-dir install -r requirements.txt
COPY app.py .
#COPY names_generator.py .
ENTRYPOINT [ "python3", "app.py" ]
CMD [ "blue" ]

# This application was named bottle originally because it was using the bottle web framework.
# It has migrated to FastAPI since then.


import sys
import os
import platform
import datetime
import json
import random
import uuid
import uvicorn
import requests
from threading import Lock
from fastapi import FastAPI, Response, HTTPException, Depends, Request
from fastapi.responses import JSONResponse, PlainTextResponse, RedirectResponse


try:
    from names_generator import get_random_name
except ImportError:
    def get_random_name():
        return 'boring_wozniak'


def make_datum(url):
    return {
        "date": datetime.datetime.utcnow(),
        "hostname": platform.node(),
        "color": color,
        "url": url,
        "random_name": get_random_name(),
        "env": os.getenv('MY_ENV', "unset"),
        "arg": os.getenv('MY_ARG', "unset"),
    }


n = 0
next_n_url = os.getenv('NEXT_N_URL', 'http://127.0.0.1:8080')
app = FastAPI()


@app.get('/favicon.ico')
def get_favicon():
    """return favicon.ico directly"""

    media_type = 'image/x-icon'
    favicon = "data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQEAYAAABPYyMiAAAABmJLR0T///////8JWPfcAAAACXBIWXMAAABIAAAASABGyWs+AAAAF0lEQVRIx2NgGAWjYBSMglEwCkbBSAcACBAAAeaR9cIAAAAASUVORK5CYII="

    return Response(content=favicon, media_type=media_type)
    

@app.route("/")
def get_home(req: Request):

    return RedirectResponse(f"{req['type']}://{req.headers['host']}/v1/n")


@app.get("/healthz")
def healthz():
    if random.randint(1, 10) < 3:
        return JSONResponse (status_code = 500, content = { "status": "Internal Server Error"})
    else:
        return JSONResponse (status_code = 200, content = { "status": "ok"})


@app.get('/v1/n')
def next_n():
    global n

    lock = Lock()
    with lock:
        n += 1
    
    return {"n": n}


@app.get("/v1/bottle/{url}")
def api(url: str):

    datum = make_datum(url)
    datum["_id"] = f'{uuid.uuid4()}'
    
    r = requests.get(next_n_url)
    datum["n"] = r.json()["n"]
    
    return datum


color = 'white'

if len(sys.argv) > 1:
    color = sys.argv[1]

if __name__ == "__main__":
    uvicorn.run("app:app", reload=True, host="0.0.0.0", port=8080)
